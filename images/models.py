from django.db import models

def image_directory_path(instance, filename):
  return "images/{0}/{1}".format(instance.section, filename)


# Create your models here.
class SectionImage(models.Model):
  title = models.CharField(max_length=30)
  project = models.ForeignKey('frontend.Project', on_delete=models.CASCADE, related_name='section_images')

  def __str__(self):
    return self.title

  def get_absolute_url(self):
    return "/{}/caja_negra/imagenes".format(self.project_id)


class Image(models.Model):
  title = models.CharField(max_length=30)
  file = models.FileField(upload_to=image_directory_path)
  shiftX = models.IntegerField(null=True, blank=True)
  shiftY = models.IntegerField(null=True, blank=True)
  section = models.ForeignKey(SectionImage, on_delete=models.CASCADE, related_name='images')

  def get_absolute_url(self):
    return "/{}/caja_negra/imagenes/{}".format(self.section.project_id, self.id)