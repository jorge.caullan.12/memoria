from django.urls import path

from . import views

urlpatterns = [
    path('create', views.ImageCreate.as_view()),
    path('update/<int:pk>', views.ImageUpdate.as_view()),
    path('delete/<int:pk>', views.ImageDelete.as_view()),

    path('section/create', views.SectionImageCreate.as_view()),
    path('section/delete/<int:pk>', views.SectionImageDelete.as_view()),
]
