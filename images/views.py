from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Image, SectionImage

# Create your views here.
class ImageCreate(CreateView): 
    model = Image
    fields = ['title', 'file', 'section']


class ImageUpdate(UpdateView): 
    model = Image
    fields = ['title', 'shiftX', 'shiftY']


class ImageDelete(DeleteView): 
    model = Image



class SectionImageCreate(CreateView): 
    model = SectionImage
    fields = ['title', 'project']


class SectionImageDelete(DeleteView): 
    model = SectionImage

