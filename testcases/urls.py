from django.urls import path

from . import views

urlpatterns = [
    path('images', views.ImageList.as_view(), name='image_list'),
    path('image/<int:pk>', views.ImageDetail.as_view(), name='image_detail'),
    path('create', views.ImageCreate.as_view(), name='image_create'),
    path('update/<int:pk>', views.ImageUpdate.as_view(), name='image_update'),
    path('delete/<int:pk>', views.ImageDelete.as_view(), name='image_delete'),

    path('section_images', views.SectionImageList.as_view(), name='section_image_list'),
    path('section_image/<int:pk>', views.SectionImageDetail.as_view(), name='section_image_detail'),
    path('create', views.SectionImageCreate.as_view(), name='section_image_create'),
    path('update/<int:pk>', views.SectionImageUpdate.as_view(), name='section_image_update'),
    path('delete/<int:pk>', views.SectionImageDelete.as_view(), name='section_image_delete'),
]
