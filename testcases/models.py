from django.contrib.postgres.fields import ArrayField
from django.db import models

# Create your models here.
class SectionTestCase(models.Model):
  title = models.CharField(max_length=30)

  project = models.ForeignKey('frontend.Project', on_delete=models.CASCADE)

  def __str__(self):
    return self.title


class TestCase(models.Model):
  title = models.CharField(max_length=30)

  section = models.ForeignKey(SectionTestCase, on_delete=models.CASCADE)


class TestCaseStep(models.Model):
  STEP_TYPES = (
    ('write', 'Write'),
    ('click', 'Click'),
    ('keys', 'Keys'),
  )

  order = models.IntegerField()
  stepType = models.CharField(
    max_length=10,
    choices=STEP_TYPES,
    default='write',
  )
  description = models.CharField(max_length=300)
  keys = ArrayField(
    models.CharField(max_length=15, blank=True),
    size=5,
  )
  content = models.CharField(max_length=300)

  testCase = models.ForeignKey(TestCase, on_delete=models.CASCADE)
  image = models.ForeignKey('images.Image', on_delete=models.CASCADE)
