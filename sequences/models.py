from django.db import models

# Create your models here.
class SectionSequence(models.Model):
  LIST_TYPE = (
    ('query', 'Query'),
    ('reading', 'Reading'),
  )

  title = models.CharField(max_length=40)
  sequenceType = models.CharField(
    max_length=10,
    choices=LIST_TYPE,
    default='query',
  )

  project = models.ForeignKey('frontend.Project', on_delete=models.CASCADE, related_name='section_sequences')

  def __str__(self):
    return self.title

  def get_absolute_url(self):
    if self.sequenceType == 'reading':
      return "/{}/caja_gris/lectura/secuencia".format(self.project_id)
    else:
      return "/{}/caja_gris/escritura/secuencia".format(self.project_id)


class Sequence(models.Model):
  title = models.CharField(max_length=60)
  times = models.IntegerField(default=1)
  updated_at = models.DateTimeField(auto_now=True)

  section = models.ForeignKey(SectionSequence, on_delete=models.CASCADE, related_name='sequences')

  def get_absolute_url(self):
    if self.section.sequenceType == 'reading':
      return "/{}/caja_gris/lectura/secuencia/{}".format(self.section.project_id, self.id)
    else:
      return "/{}/caja_gris/escritura/secuencia/{}".format(self.section.project_id, self.id)


class SequenceStep(models.Model):
  order = models.IntegerField()

  sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE, related_name='sequence_steps')
  query = models.ForeignKey('queries.Query', on_delete=models.CASCADE, related_name='sequence_steps', blank=True, null=True)
  reading = models.ForeignKey('readings.Reading', on_delete=models.CASCADE, related_name='sequence_steps', blank=True, null=True)
