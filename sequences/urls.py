from django.urls import path

from . import views

urlpatterns = [
    path('create', views.SequenceCreate.as_view()),
    path('update/<int:pk>', views.SequenceUpdate.as_view()),
    path('delete/<int:pk>', views.SequenceDelete.as_view()),

    path('section/create', views.SectionSequenceCreate.as_view()),
    path('section/delete/<int:pk>', views.SectionSequenceDelete.as_view()),

    path('save_steps/<int:sequence_id>', views.save_steps),
    path('execute_readings/<int:sequence_id>', views.execute_readings),
    path('execute_queries/<int:sequence_id>', views.execute_queries),
]
