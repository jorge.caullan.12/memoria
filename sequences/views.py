from django.contrib import messages
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import SectionSequence, Sequence, SequenceStep
from django.contrib.messages.views import SuccessMessageMixin
from readings.models import Reading
from readings.views import get_test_workshet
from queries.models import Query
from queries.views import get_query_response

from openpyxl import Workbook
import datetime

# Create your views here.
class SectionSequenceCreate(SuccessMessageMixin, CreateView):
  model = SectionSequence
  fields = ['title', 'project', 'sequenceType']
  success_message = "Seccion de secuencias creada correctamente"

class SectionSequenceDelete(DeleteView):
  model = SectionSequence



class SequenceCreate(SuccessMessageMixin, CreateView):
  model = Sequence
  fields = ['title', 'section']
  success_message = "Secuencia de pruebas creada correctamente"


class SequenceUpdate(UpdateView):
  model = Sequence
  fields = ['title', 'section']


class SequenceDelete(DeleteView):
  model = Sequence
  def delete(self, request, *args, **kwargs):
    self.get_object().delete()
    messages.success(self.request, 'Secuencia eliminada correctamente')
    return JsonResponse({'deleted': 'ok'})



def save_steps(request, sequence_id):
  sequence = Sequence.objects.get(id=sequence_id)
  steps = request.POST.getlist('steps[]')
  # print(steps)

  if sequence.section.sequenceType == 'reading':
    for i in range(len(steps)):
      reading = Reading.objects.get(id=steps[i])
      step = sequence.sequence_steps.filter(order=i+1)
      if step.exists():
        step = step.last()
        step.reading = reading
      else:
        step = SequenceStep(reading=reading, order=i+1, sequence=sequence)

      step.save()
      
  elif sequence.section.sequenceType == 'query':
    for i in range(len(steps)):
      query = Query.objects.get(id=steps[i])
      step = sequence.sequence_steps.filter(order=i+1)
      if step.exists():
        step = step.last()
        step.query = query
      else:
        step = SequenceStep(query=query, order=i+1, sequence=sequence)

      step.save()
    # for s in sequence.sequence_steps.all():
    #   print(s.query_id)

  # borrar los steps sobrantes
  if len(steps) < sequence.sequence_steps.count():
    for i in range(len(steps), sequence.sequence_steps.count()):
      step = sequence.sequence_steps.filter(order=i+1).last()
      step.delete()

  sequence.save() #touch para guardarlo como ultimo editado

  messages.success(request, 'Secuencia guardada correctamente')
  return JsonResponse({'update': 'ok'})


def execute_readings(request, sequence_id):
  sequence = Sequence.objects.get(id=sequence_id)

  steps = sequence.sequence_steps.all()

  wb = Workbook()
  wb.remove_sheet(wb.active)

  for step in steps:
    test_id = step.reading_id
    test = Reading.objects.get(id=test_id)

    if test.reading_variables.count() > 0:
      wb, response = get_test_workshet(test_id, wb)


  filename = "{} {}.xlsx".format(sequence.title, datetime.datetime.now().strftime('%d%m%Y_%I%p'))

  response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
  response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
  # messages.success(request, 'Secuencia ejecutada correctamente')
  wb.save(response)
  return response


# solo ejecutar la secuencia de test  y por cada uno cuando este erroneo enviar un message (notificacion) 
def execute_queries(request, sequence_id):
  sequence = Sequence.objects.get(id=sequence_id)

  steps = sequence.sequence_steps.all()

  n_satisfied = 0
  for step in steps:
    test_id = step.query_id
    test = Query.objects.get(id=test_id)

    r = get_query_response(test_id, request, False)
    if r['satisfied'] == False:
      messages.error(request, 'La consulta "{}" posee problemas de validaciones (ir a la consulta para ver detalles)'.format(test.title))
    else:
      n_satisfied += 1

  messages.success(request, 'Secuencia ejecutada correctamente<br><br>{}/{} pruebas correctas'.format(n_satisfied, steps.count()))

  # return response
  return JsonResponse({'satisfied': 'ok'})