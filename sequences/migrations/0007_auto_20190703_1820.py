# Generated by Django 2.2 on 2019-07-03 22:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sequences', '0006_sequence_sequencetype'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sequence',
            name='sequenceType',
            field=models.CharField(choices=[('query', 'Query'), ('reading', 'Reading')], default='query', max_length=15),
        ),
    ]
