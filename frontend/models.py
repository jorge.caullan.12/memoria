from django.db import models

# Create your models here.
class Project(models.Model):
  name = models.CharField(max_length=40)

  def __str__(self):
    return self.name


class EnvironmentValue(models.Model):
  ENVIRONMENT_TYPES = (
    ('domain', 'Domain'),
    ('variable', 'Variable'),
  )

  environmentType = models.CharField(
    max_length=10,
    choices=ENVIRONMENT_TYPES,
    default='variable',
  )
  variable = models.CharField(max_length=40)
  value = models.TextField()

  project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='environments')

