from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import render, redirect
from .models import Project, EnvironmentValue
from images.models import *
import json, re, datetime
from queries.views import get_full_url
from queries.models import SectionQuery, Query, QueryExpectedData, QueryResponse, QueryData
from sequences.models import SectionSequence, Sequence, SequenceStep
from readings.models import SectionReading, Reading, ReadingVariables, ReadingValidation

def get_sections_and_tests(initial_sections, testType):
  items = []
  section_names = []
  sections = initial_sections
  # obtener los test de cada seccion
  for section in sections:
    # limpieza de secciones repetidas
    if section.title in section_names:
      sections = sections.exclude(id=section.id)
      section.delete()
      continue
    else:
      section_names.append(section.title)

    item = {}
    item['id'] = section.id
    item['section'] = section.title

    item['tests'] = []
    if testType == 'reading':
      tests = section.readings.all()
    elif testType == 'query':
      tests = section.queries.all()
    else:
      tests = section.sequences.all()

    for test in tests:
      item['tests'].append({
          'id': test.id,
          'title': test.title
        })

    if len(item['tests']) > 0:
      items.append(item)

  return (sections, items)


# Create your views here.
def index(request):
  projects = Project.objects.all().order_by('-id')

  data = {
    'projects': projects
  }
  return render(request, 'menus/index.html', data)


def menu_tipo_automatizacion(request, project_id):
  data = {
    'title': 'Seleccionar el tipo de automatización',
    'buttons': {
      'first': {
        'name': 'Pruebas de Caja Gris',
        'icon': 'code-solid.svg',
        'tooltip': 'Las pruebas de caja gris están enfocadas en automatizar pruebas de software sin utilizar una interfaz usuaria. Por ejemplo, acceder a la base de datos para comprobar la lógica del negocio o acceder directamente al "Back End" de la aplicación.',
        'href': 'caja_gris'
      },
      'second': {
        'name': 'Pruebas de Caja Negra',
        'icon': 'window-restore-regular.svg',
        'tooltip': 'Las pruebas de caja negra se especializan en probar el funcionamiento de las interfaces usuarias para la realización de pruebas de "Front End". Por ejemplo, la realización de un flujo ingresando los datos de un usuario para iniciar sesión.',
        'href': 'caja_negra'
      },
      'last': {
        'name': 'Generar Documentación',
        'tooltip': 'La generación de reportes permite ayudar a los desarrolladores, mostrándole los resultados de las pruebas que han sido ejecutadas o generando documentación de las automatizaciones que ya fueron realizadas.',
        'href': 'reportes'
      }
    },
    'back': '/'
  }
  return render(request, 'menus/tipo_automatizacion.html', data)


def menu_caja_gris(request, project_id):
  data = {
    'title': 'Automatizaciones de Caja Gris',
    'buttons': {
      'first': {
        'name': 'Pruebas de Escritura',
        'icon': 'keyboard-regular.svg',
        'tooltip': 'Las pruebas de escritura se enfocan en hacer consultas directamente al "Back End" de forma que se prueben y documenten las rutas de una API, con la ventaja de además ser capaz de ejecutar secuencias de consultas con la finalidad de probar ágilmente un conjunto de consultas ya construidas.',
        'href': 'caja_gris/escritura/consulta'
      },
      'sub_first': {
        'name': 'Ejecutar secuencias',
        'href': 'caja_gris/escritura/secuencia'
      },
      'second': {
        'name': 'Pruebas de Lectura',
        'icon': 'terminal-solid.svg',
        'tooltip': 'Las pruebas de lectura se enfocan principalmente en la lectura de la base de datos, enfocándose en comprobar que la lógica del negocio se cumple correctamente al igual que la consistencia de los datos luego de las transacciones.',
        'href': 'caja_gris/lectura'
      },
      'sub_second': {
        'name': 'Ejecutar secuencias',
        'href': 'caja_gris/lectura/secuencia'
      }
    },
    'back': 'tipo_automatizacion'
  }
  return render(request, 'menus/tipo_automatizacion.html', data)


def menu_caja_negra(request, project_id):
  data = {
    'title': 'Automatizaciones de Caja Negra',
    'buttons': {
      'first': {
        'name': 'Imágenes',
        'icon': 'image-regular.svg',
        'tooltip': 'En esta sección se podrán subir imágenes representativas de un botón o sección de la interfaz usuaria, con la cual mediante reconocimiento de imágenes se podrá simular hacer clics.<br><b>Importante:</b> Las imágenes deben ser únicas y representativas <br>(ej: un cuadro blanco no es útil para seleccionar un formulario).',
        'href': 'caja_negra/imagenes'
      },
      'second': {
        'name': 'Casos de Prueba',
        'icon': 'laptop-code-solid.svg',
        'tooltip': 'En esta sección se puede definir cada funcionalidad, especificando el paso a paso que debe seguir ésta para completarse correctamente. Cada prueba permite: Hacer clics, Presionar combinaciones de teclas, Escribir texto.',
        'href': 'caja_negra/casos_prueba'
      }
    },
    'back': 'tipo_automatizacion'
  }
  return render(request, 'menus/tipo_automatizacion.html', data)


def menu_reportes(request, project_id):
  data = {'back': 'tipo_automatizacion', 'project_id': project_id}
  project = Project.objects.get(id=project_id)

  test_queries = []
  for section_query in project.section_queries.all():
    queries = section_query.queries.filter(status='built')
    if queries.count() > 0 and section_query.title not in test_queries:
      test_queries.append({
        'name': section_query.title,
        'value': section_query.id,
        'childrens': queries,
      })
      # for query in queries:
      #   section['childrens'].append(query)

  test_readings = []
  for section_reading in project.section_readings.all():
    readings = section_reading.readings.filter(status='built')
    if readings.count() > 0 and section_reading.title not in test_readings:
      test_readings.append({
        'name': section_reading.title,
        'value': section_reading.id,
        'childrens': readings,
      })
      # for reading in readings:
      #   section['childrens'].append(reading)


  data['tests'] = [
    {
      'name': 'Pruebas de Caja Gris',
      'value': 'caja_gris',
      'childrens': [
        {
          'name': 'Pruebas de Escritura',
          'value': 'queries',
          'childrens': test_queries,
        },
        {
          'name': 'Pruebas de Lectura',
          'value': 'readings',
          'childrens': test_readings,
        },
      ],
    },
  ]

  return render(request, 'menus/reportes.html', data)


def caja_negra_imagenes(request, project_id, test_id = 0):
  data = {'items': []}

  if test_id == 0:
    last_used_id = 1 #aqui se debe buscar en la bd el utilimo utilizado
    return redirect('imagenes/{}'.format(last_used_id))
  data['test_id'] = test_id
  # image = Image.objects.get_or_create(id=test_id)

  project = Project.objects.get(id=project_id)
  sections = project.section_images.all()
  data['sections'] = sections

  for section in sections:
    item = {}
    item['id'] = section.id
    item['section'] = section.title

    item['tests'] = []
    tests = section.images.all()
    for test in tests:
      item['tests'].append({
          'id': test.id,
          'title': test.title
        })

    data['items'].append(item)

  data['project_id'] = project_id

  # left sidebar
  data['buttons'] = {
    'first': {
      'name': 'Imágenes',
      'selected': True,
      'href': '/'+str(project_id)+'/caja_negra/imagenes'
    },
    'second': {
      'name': 'Casos de Prueba',
      'href': '/'+str(project_id)+'/caja_negra/casos_prueba'
    },
    'last': {
      'name': 'Agregar Nueva Imagen'
    }
  }
  data['items_title'] = 'Imágenes del proyecto'
  data['add_section'] = {
    "title": 'Añadir Sección a Imágenes',
    "placeholder": 'Nombre de la nueva sección de Imágenes',
    "href": '/images/section/create'
  }

  data['title'] = 'Imagen: Boton Iniciar Sesion'

  return render(request, 'caja_negra/imagenes.html', data)


def caja_negra_casos_prueba(request, project_id, test_id = 0):
  data = {}
  data['items'] = [
    {
      'section': 'Nombre de la primera sección',
      'id': 1,
      'tests': [
        {
          'title': 'Primer test',
          'id': 1
        },
        {
          'title': 'Segundo test',
          'id': 2
        }
      ]
    },
    {
      'section': 'Nombre de la segunda sección',
      'id': 2,
      'tests': [
        {
          'title': 'Tercer test',
          'id': 3
        }
      ]
    },
    {
      'id': 3,
      'section': 'Nombre de la tercera sección',
      'tests': [
        {
          'id': 4,
          'title': 'Cuarto test',
        },
        {
          'id': 5,
          'title': 'Quinto test',
        }
      ]
    }]

  if test_id == 0:
    last_used_id = 1 #aqui se debe buscar en la bd el utilimo utilizado
    return redirect('casos_prueba/{}'.format(last_used_id))
  data['test_id'] = test_id

  data['project_id'] = project_id

  # left sidebar
  data['buttons'] = {
    'first': {
      'name': 'Imágenes',
      'href': '/'+str(project_id)+'/caja_negra/imagenes'
    },
    'second': {
      'name': 'Casos de Prueba',
      'selected': True,
      'href': '/'+str(project_id)+'/caja_negra/casos_prueba'
    },
    'last': {
      'name': 'Añadir una Prueba'
    }
  }
  data['items_title'] = 'Casos de prueba del proyecto'
  data['add_section'] = {
    "title": 'Añadir Sección a Casos de prueba',
    "placeholder": 'Nombre de la nueva sección de Casos de prueba'
  }

  # selected test #hacer un diccionario que traduzca los tipos {'clic':'Hacer Click', 'press':''}
  data['steps'] = [
    {
      'order': 1,
      'type': 'Hacer Clic',
      'delay': 0,
      'item':{
        'href': '/static/assets/button_example.png',
        'clickX': 10,
        'clickY': 0
      },
      'description': "Hacer click en el boton inicial para añadir una imagen"
    },
    {
      'order': 2,
      'type': 'Combinación de Teclas',
      'delay': 0,
      'item':{
        'buttons': " + ".join(map(lambda x: x.upper(), ["Alt","Tab"]))
      }
    },
    {
      'order': 3,
      'type': 'Escribir',
      'delay': 0,
      'item':{
        'text': 'Esto es lo que se esta escribiendo'
      }
    },
    {
      'order': 4,
      'type': 'Escribir',
      'delay': 0,
      'item':{
        'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam'
      }
    },
    {
      'order': 5,
      'type': 'Hacer Clic',
      'delay': 0,
      'item':{
        'href': '/static/assets/button_example.png',
        'clickX': 0,
        'clickY': 0
      },
      'description': ""
    },
  ]

  data['title'] = 'Caso de prueba: Iniciar Sesion'

  return render(request, 'caja_negra/casos_prueba.html', data)


def caja_gris_lectura(request, project_id, test_id = 0):
  data = {'items': []}
  data['test_type'] = 'readings'

  data['test_id'] = test_id
  data['project_id'] = project_id
  project = Project.objects.get(id=project_id)

  initial_sections = project.section_readings.all().order_by('id') #en este caso es importante que esten ordenadas para que se eliminen las ultimas y nunca las primeras
  data['sections'], data['items'] = get_sections_and_tests(initial_sections, 'reading')

  # left sidebar
  data['buttons'] = {
    'last': {
      'name': 'Añadir una Prueba'
    }
  }
  data['items_title'] = 'Pruebas de lectura del proyecto'
  data['add_section'] = {
    "title": 'Añadir Sección a Pruebas de lectura',
    "placeholder": 'Nombre de la nueva sección de pruebas de lectura',
    "href": '/readings/section/create'
  }

  data['create_test'] = {
    'title': 'Agregar una prueba de lectura',
    'href': '/readings/create',
    'placeholder': 'Ej: Verificacion de Saldos'
  }
  
  if test_id == 0 or not Reading.objects.filter(id=test_id, section__project_id=project_id).exists():
    last_used = Reading.objects.filter(section__project_id=project_id).order_by('updated_at').last()
    if last_used:
      return redirect('/{}/caja_gris/lectura/{}'.format(project_id, last_used.id))
    else:
      data['title'] = 'No hay pruebas de lecturas creadas, por favor crea una prueba y una seccion que la contenga'
      return render(request, 'sub_templates/without_tests.html', data)
  this_test = Reading.objects.get(id=test_id)
  data['test'] = this_test

  data['all_databases'] = [
    {
      'name': 'Oracle',
      'value': 'oracle'
    },
    {
      'name': 'Postgres',
      'value': 'postgresql'
    },
  ]

  if this_test.reading_variables.count() > 0:
    data['variables'] = this_test.reading_variables.last()
    data['test_executed'] = True

  data['title'] = 'Prueba de lectura: {}'.format(this_test.title)

  data['existant_environment_variables'] = []
  for environment in project.environments.filter(environmentType='variable'):
    data['existant_environment_variables'].append({
        'var': environment.variable,
        'val': environment.value
      })

  if this_test.username != '' and this_test.service != '':
    data['test_conection'] = 'ok'
  else:
    data['test_conection'] = 'error'


  all_validations = {
    'comparison_validations': [],
    'sequential_validations': [],
  }

  for validation in this_test.reading_validations.all().order_by('-id'):
    validation_data = {
        'id': validation.id,
        'logic_type': 'Codigo Python',
        'variable': validation.variable,
        'description': validation.description,
        'data': {
          'python_code': validation.code.split("\n"),
          'true_color': validation.trueColor,
          'true_comment': validation.trueComment,
          'false_color': validation.falseColor,
          'false_comment': validation.falseComment
        }
      }

    if validation.validationType == 'comparison':
      all_validations['comparison_validations'].append(validation_data)
    elif validation.validationType == 'sequential':
      all_validations['sequential_validations'].append(validation_data)

  data['validations'] = all_validations

  return render(request, 'caja_gris/lectura.html', data)


def caja_gris_lectura_secuencia(request, project_id, test_id = 0):
  data = {'items': []}
  data['test_type'] = 'sequences'

  data['project_id'] = project_id

  project = Project.objects.get(id=project_id)

  initial_sections = project.section_sequences.filter(sequenceType='reading').order_by('id')
  data['sections'], data['items'] = get_sections_and_tests(initial_sections, 'sequence')

  # left sidebar
  data['buttons'] = {
    'first': {
      'name': 'Pruebas de lectura',
      'href': '/'+str(project_id)+'/caja_gris/lectura'
    },
    'second': {
      'name': 'Secuencias',
      'selected': True,
      'href': '/'+str(project_id)+'/caja_gris/lectura/secuencia'
    },
    'last': {
      'name': 'Añadir una Secuencia'
    }
  }
  data['items_title'] = 'Secuencias de lecturas'
  data['add_section'] = {
    "title": 'Añadir Sección a Secuencias de Pruebas de lecturas',
    "placeholder": 'Nombre de la nueva sección de secuencias de lectura',
    "type": 'reading',
    "href": '/sequences/section/create',
  }

  data['create_test'] = {
    'title': 'Crear una nueva secuencia',
    'href': '/sequences/create',
    'placeholder': 'Ej: Bases de datos principales'
  }
  
  if test_id == 0 or not Sequence.objects.filter(id=test_id, section__project_id=project_id).exists():
    last_used = Sequence.objects.filter(section__sequenceType='reading', section__project_id=project_id).order_by('updated_at').last()
    if last_used:
      return redirect('/{}/caja_gris/lectura/secuencia/{}'.format(project_id, last_used.id))
    else:
      data['title'] = 'No hay secuencias de lecturas creadas, por favor crea una secuencia y una seccion que la contenga'
      return render(request, 'sub_templates/without_tests.html', data)
  data['test_id'] = test_id

  this_test = Sequence.objects.get(id=test_id)
  data['test'] = this_test
  data['title'] = 'Secuencia de pruebas de lectura: {}'.format(this_test.title)

  # ids de los tests
  data['steps'] = []
  for step in this_test.sequence_steps.all():
    data['steps'].append(step)

  # data para mostrar en los selects, se usan los tests de readings
  all_tests = {}
  for section in project.section_readings.all():
    for test in section.readings.exclude(service='').exclude(sql=''):
      if section.title not in all_tests:
        all_tests[section.title] = []
      all_tests[section.title].append({
        'id': test.id,
        'title': test.title,
        'service': test.service,
        'username': test.username
      })

  data['all_tests'] = all_tests

  return render(request, 'caja_gris/lectura_secuencia.html', data)


def caja_gris_escritura_consulta(request, project_id, test_id = 0):
  data = {'items': []}
  data['test_type'] = 'queries'
  data['test_id'] = test_id
  data['project_id'] = project_id

  project = Project.objects.get(id=project_id)
  initial_sections = project.section_queries.all()

  data['sections'], data['items'] = get_sections_and_tests(initial_sections, 'query')

  # options
  data['options_methods'] = [
    {'var':'GET', 'val': 'get'}, 
    {'var':'POST', 'val': 'post'}, 
    {'var':'PUT', 'val': 'put'}, 
    {'var':'PATCH', 'val': 'patch'}, 
    {'var':'DELETE', 'val': 'delete'}
  ]
  data['options_expected_responses'] = [
    {'var': 'Existe', 'val':'exist'},
    {'var': 'Es Nulo', 'val':'is_null'},
    {'var': 'Es Texto', 'val':'is_text'},
    {'var': 'Es un Numero', 'val':'is_number'},
    {'var': 'Es una Lista', 'val':'is_list'},
    {'var': 'Es un Diccionario', 'val':'is_dictionary'},
    {'var': 'Igual a', 'val':'equal'},
    {'var': 'Distinto a', 'val':'not_equal'},
    {'var': 'Mayor a', 'val':'greather_than'},
    {'var': 'Mayor o igual a', 'val':'greather_equal_than'},
    {'var': 'Menor a', 'val':'smaller_than'},
    {'var': 'Menor o igual a', 'val':'smaller_equal_than'},
    {'var': 'Contiene', 'val':'include'}
  ]

  # left sidebar
  data['buttons'] = {
    'first': {
      'name': 'Consultas',
      'selected': True,
      'href': '/'+str(project_id)+'/caja_gris/escritura/consulta'
    },
    'second': {
      'name': 'Secuencias',
      'href': '/'+str(project_id)+'/caja_gris/escritura/secuencia'
    },
    'last': {
      'name': 'Añadir una Consulta'
    }
  }
  data['items_title'] = 'Lista de rutas del proyecto'
  data['add_section'] = {
    "title": 'Añadir Sección a las rutas del proyecto',
    "placeholder": 'Nombre de la nueva sección de Consultas',
    "href": '/queries/section/create'
  }

  data['create_test'] = {
    'title': 'Crear una nueva consulta',
    'href': '/queries/create',
    'placeholder': 'Ej: Iniciar Sesión'
  }
  
  if test_id == 0 or not Query.objects.filter(id=test_id, section__project_id=project_id).exists():
    last_used = Query.objects.filter(section__project_id=project_id).order_by('updated_at').last()
    if last_used:
      return redirect('/{}/caja_gris/escritura/consulta/{}'.format(project_id, last_used.id))
    else:
      data['title'] = 'No hay pruebas de escritura creadas, por favor crea una prueba y una seccion que la contenga'
      return render(request, 'sub_templates/without_tests.html', data)

  this_test = Query.objects.get(id=test_id)
  data['test'] = this_test

  # if hasattr(this_test, 'response'):
  data['test_executed'] = True #siempre se puede construir (con esto, sirve para generar TDD)

  #headers dinamic form
  data['existant_headers'] = []
  for header in this_test.query_datas.filter(dataType='header'):
    data['existant_headers'].append({
        'var': header.variable,
        'val': header.value,
        'desc': header.description
      })

  data['existant_body_form_datas'] = []
  for form in this_test.query_datas.filter(dataType='form'):
    data['existant_body_form_datas'].append({
        'var': form.variable,
        'val': form.value,
        'desc': form.description,
        'file': form.file
      })

  existant_json = this_test.query_datas.filter(dataType='json').last()
  if existant_json:
    data['existant_json_data'] = existant_json.json

  data['existant_expected_responses'] = []
  for expected_data in this_test.query_expected_datas.all().order_by('id'): #si no se ordena por id, aparecen inversas
    data['existant_expected_responses'].append({
        'var': expected_data.variable,
        'comp': expected_data.comparison,
        'val': expected_data.value,
        'satisfied': expected_data.satisfied
      })


  # VARIABLES DE ENTORNO
  data['existant_domains'] = []
  for environment in project.environments.filter(environmentType='domain'):
    data['existant_domains'].append({
        'var': environment.variable,
        'val': environment.value
      })
  data['existant_environment_variables'] = []
  for environment in project.environments.filter(environmentType='variable'):
    data['existant_environment_variables'].append({
        'var': environment.variable,
        'val': environment.value
      })

  data['title'] = 'Consulta de prueba de escritura: {}'.format(this_test.title)

  return render(request, 'caja_gris/escritura_consulta.html', data)


def caja_gris_escritura_secuencia(request, project_id, test_id = 0):
  data = {'items': []}
  data['test_type'] = 'sequences'

  data['project_id'] = project_id

  project = Project.objects.get(id=project_id)

  initial_sections = project.section_sequences.filter(sequenceType='query').order_by('id')
  data['sections'], data['items'] = get_sections_and_tests(initial_sections, 'sequence')

  # left sidebar
  data['buttons'] = {
    'first': {
      'name': 'Consultas',
      'href': '/'+str(project_id)+'/caja_gris/escritura/consulta'
    },
    'second': {
      'name': 'Secuencias',
      'selected': True,
      'href': '/'+str(project_id)+'/caja_gris/escritura/secuencia'
    },
    'last': {
      'name': 'Añadir una Secuencia'
    }
  }
  data['items_title'] = 'Secuencias de consultas'
  data['add_section'] = {
    "title": 'Añadir Sección a Secuencias de Consultas',
    "placeholder": 'Nombre de la nueva sección de Secuencias',
    "type": 'query',
    "href": '/sequences/section/create'
  }

  data['create_test'] = {
    'title': 'Crear una nueva secuencia',
    'href': '/sequences/create',
    'placeholder': 'Ej: Login & Logout'
  }

  if test_id == 0 or not Sequence.objects.filter(id=test_id, section__project_id=project_id).exists():
    last_used = Sequence.objects.filter(section__sequenceType='query', section__project_id=project_id).order_by('updated_at').last()
    if last_used:
      return redirect('/{}/caja_gris/escritura/secuencia/{}'.format(project_id, last_used.id))
    else:
      data['title'] = 'No hay secuencias de escritura creadas, por favor crea una secuencia y una seccion que la contenga'
      return render(request, 'sub_templates/without_tests.html', data)
  data['test_id'] = test_id

  this_test = Sequence.objects.get(id=test_id)
  data['test'] = this_test
  data['title'] = 'Secuencia de pruebas de escritura: {}'.format(this_test.title)

  # ids de los tests
  data['steps'] = []
  for step in this_test.sequence_steps.all():
    data['steps'].append(step)

  # data para mostrar en los selects, se usan los tests de readings
  all_routes = {}
  for section in project.section_queries.all():
    for test in section.queries.exclude(domain=None):
      if section.title not in all_routes:
        all_routes[section.title] = []

      url = get_full_url(test.domain.value, test.route)
      all_routes[section.title].append({
        'id': test.id,
        'title': test.title,
        'method': test.method.upper(),
        'route': url
      })

  data['all_routes'] = all_routes

  return render(request, 'caja_gris/escritura_secuencia.html', data)





def update_environment_variables(request, project_id):
  # print(request.POST)
  project = Project.objects.get(id=project_id)

  deleted_environments = project.environments.filter(project_id=project.id) #todos se 'eliminan' hasta que se compruebe su uso
  new_environments = []

  for key in request.POST:
    if key == 'clean_domains' or key == 'clean_variables':
      continue
    splited_key = key.replace(']','').split('[')

    environment = project.environments.filter(environmentType=splited_key[0], variable=splited_key[1], value=request.POST[key], project_id=project.id).last()
    if environment:
      #el elemento aun existe, por lo que no hay que borrarlo
      deleted_environments = deleted_environments.exclude(id=environment.id)
    else:
      environment = EnvironmentValue(project=project)
      environment.environmentType = splited_key[0]
      environment.variable = splited_key[1]
      environment.value = request.POST[key]
      new_environments.append(environment)
    # EnvironmentValue.objects.create(environmentType=splited_key[0], variable=splited_key[1], value=request.POST[key], project=project)


  if project.environments.count() > 0:
    if 'clean_domains' in request.POST:
      deleted_environments.filter(environmentType='domain').delete()

    if 'clean_variables' in request.POST:
      deleted_environments.filter(environmentType='variable').delete()
    
  for environment in new_environments:
    environment.save()

  domains = []
  for env in project.environments.filter(environmentType='domain'):
    env_dict = env.__dict__
    del env_dict['_state']
    del env_dict['environmentType']
    domains.append(env_dict)
  variables = []
  for env in project.environments.filter(environmentType='variable'):
    env_dict = env.__dict__
    del env_dict['_state']
    del env_dict['environmentType']
    variables.append(env_dict)

  return JsonResponse({'domains': domains, 'variables': variables})


def generate_reports(request, project_id):
  project = Project.objects.get(id=project_id)

  readings = []
  queries = []
  show_detail = False
  show_results = False

  added_readings_sections_dict = {}
  added_queries_sections_dict = {}
  for sended_data in request.POST:
    if 'readings' in sended_data:
      testType, test_id = sended_data.split('__')
      test = Reading.objects.get(id=test_id)
      if test.section.id not in added_readings_sections_dict:
        added_readings_sections_dict[test.section.id] = len(readings)
        readings.append({
          'name': test.section.title,
          'childrens': []
        })
      readings[added_readings_sections_dict[test.section.id]]['childrens'].append(test)
    elif 'queries' in sended_data:
      testType, test_id = sended_data.split('__')
      test = Query.objects.get(id=test_id)
      if test.section.id not in added_queries_sections_dict:
        added_queries_sections_dict[test.section.id] = len(queries)
        queries.append({
          'name': test.section.title,
          'childrens': []
        })
      queries[added_queries_sections_dict[test.section.id]]['childrens'].append(test)
    elif 'show_detail' in sended_data:
      show_detail = True
    elif 'show_results' in sended_data:
      show_results = True

  tests = [
    {
      'name': 'Pruebas de Caja Gris',
      'value': 'caja_gris',
      'childrens': [],
    },
  ]

  if len(queries) > 0:
    tests[0]['childrens'].append({
      'name': 'Pruebas de Escritura',
      'value': 'queries',
      'childrens': queries,
    })
  if len(readings) > 0:
    tests[0]['childrens'].append({
      'name': 'Pruebas de Lectura',
      'value': 'readings',
      'childrens': readings,
    })

  # print(readings)
  # print(queries)
  data = {
    'project': project,
    'tests': tests,
    'show_detail': show_detail,
    'show_results': show_results,
    'date': datetime.datetime.now(),
    'date_str': datetime.datetime.now().strftime('%d-%m-%Y_%I%M%p'),
  }

  return render(request, 'menus/reportes_generados.html', data)

