from django.template.defaultfilters import register
from django.conf import settings
from queries.views import replace_env_variables
from queries.models import Query, QueryResponse
from readings.models import Reading
from pytz import timezone
# from django.conf.settings import BASE_DIR
import sass, os, re, json

@register.filter(name='dict_key')
def dict_key(d, k):
  if d.get(k, '') == None:  
    return ''
  else: return d.get(k, '')

# @register.filter(name='form_key')
# def form_key(d, k):
#   if d.__getitem__(k) == None:
#     return ''
#   else:
#     return d.__getitem__(k)


@register.filter(name='css_compiler')
def css_compiler(sass_filename):
  f = open(os.path.join(settings.SCSS_ROOT, sass_filename))
  scss_text = f.read()
  f.close()

  return '<style type="text/css">'+sass.compile(string=scss_text)+'</style>'

@register.filter(name='svgColor')
def svgColor(svg_filename, color):
  f = open(os.path.join(settings.STATIC_ROOT+'/assets', svg_filename))
  svg_result = f.read()
  f.close()

  svg_result = svg_result.replace("currentColor",color)
  return svg_result


@register.filter(name='tests_ids')
def tests_ids(tests_section):
  return list(map(lambda test: test['id'], tests_section))


@register.filter(name='show_variables')
def show_variables(variables):
  response = ''
  all_variables = {}
  for var in variables:
    splited = var.split('.')
    if splited[0] not in all_variables:
      all_variables[splited[0]] = []
    all_variables[splited[0]].append(splited[1])

  for table in all_variables:
    response = response + '<b>' + table + '</b><br>'
    response = response + ', '.join(all_variables[table]) + '<br><br>'
  return response


@register.filter(name='show_variables_options')
def show_variables_options(variables):
  response = ''
  all_variables = {}
  for var in variables:
    splited = var.split('.')
    if splited[0] not in all_variables:
      all_variables[splited[0]] = []
    all_variables[splited[0]].append(var)

  for table in all_variables:
    response = response + '<optgroup label="{}"">'.format(table)
    for var in all_variables[table]:
      response = response + '<option value="{}"> {} </option>'.format(var, var.split('.')[1])
    response = response + '</optgroup>'
  return response


@register.filter(name='chevron')
def chevron(direction, color):
  f = open(os.path.join(settings.STATIC_ROOT+'/assets', 'chevron-right-solid.svg'))
  svg_result = f.read()
  f.close()
  svg_result_right = svg_result.replace("currentColor",color)

  f = open(os.path.join(settings.STATIC_ROOT+'/assets', 'chevron-down-solid.svg'))
  svg_result = f.read()
  f.close()
  svg_result_down = svg_result.replace("currentColor",color)

  if direction=='down':
    response = '<div class="chevron right" style="display: none;">'+svg_result_right+'</div><div class="chevron down">'+svg_result_down+'</div>'
  elif direction=='right':
    response = '<div class="chevron right">'+svg_result_right+'</div><div class="chevron down" style="display: none;">'+svg_result_down+'</div>'
  
  return response


@register.filter(name='stringify_data')
def stringify_data(data):
  return data.replace("\"","'").replace("\n","")


@register.filter(name='get_query_route')
def get_query_route(query, project_id):
  domain = query.domain.value
  path = query.route
  if re.search(r'^http://', domain) or re.search(r'^https://', domain):
    url = domain
  else:
    url = 'http://'+domain

  if (not re.search(r'/$', domain)) and (not re.search(r'^/', path)):
    url = '{}/{}'.format(url, path)
  else:
    url = '{}{}'.format(url, path)
  return '{} - {}'.format(query.method.upper(), replace_env_vars(url, project_id))


@register.filter(name='get_test_url')
def get_test_url(test):
  if type(test)==Query:
    return '/{}/caja_gris/escritura/consulta/{}'.format(test.section.project_id, test.id)
  elif type(test)==Reading:
    return '/{}/caja_gris/lectura/{}'.format(test.section.project_id, test.id)

# @register.filter(name='get_reading_database')
# def get_reading_database(reading):
#   return

@register.filter(name='show_query_headers')
def show_query_headers(query):
  headers = {}
  for header in query.query_datas.filter(dataType='header'):
    headers[replace_env_variables(header.variable, query.section.project_id)] = replace_env_variables(header.value, query.section.project_id)
  # return json.dumps(headers, indent=2)
  if headers != {}:
    return json.dumps(headers, indent=2)
  else:
    return 'Sin headers'

@register.filter(name='show_query_response_date')
def show_query_response_date(query):
  return " ({})".format(query.response.date.astimezone(timezone('America/La_Paz')).strftime('%d/%m/%y %I%p'))

@register.filter(name='show_query_data')
def show_query_data(query):
  form_datas = {}
  for form_data in query.query_datas.filter(dataType='form'):
    form_datas[replace_env_variables(form_data.variable, query.section.project_id)] = replace_env_variables(form_data.value, query.section.project_id)

  if form_datas == {}: #solo se lee la data form si no existe
    json_query_data = query.query_datas.filter(dataType='json').last()
    if json_query_data and json_query_data.json: # para los casosen que no se use json (esta vacio)
      # json_data = json.loads(replace_env_variables(json_query_data.json, query.section.project_id))
      # for var in json_data:
      #   form_datas[var] = json_data[var]
      form_datas = json.loads(replace_env_variables(json_query_data.json, query.section.project_id))

  if form_datas != {}:
    return json.dumps(form_datas, indent=2)
  else:
    return 'Sin data'

@register.filter(name='show_query_datatype')
def show_query_datatype(query):
  form_datas = {}
  json_elem = query.query_datas.filter(dataType='json').last()
  if query.query_datas.filter(dataType='form').count() > 0:
    return '(as form)'
  elif json_elem and json_elem.json:
    return '(as json)'
  else:
    return ''

@register.filter(name='show_reading_validations_comparisons')
def show_reading_validations_comparisons(reading):
  comparisons = []
  for validation in reading.reading_validations.filter(validationType='comparison'):
    comparisons.append({
      'variable': validation.variable,
      'description': validation.description,
      'code': replace_env_variables(validation.code, reading.section.project_id),
    })
  return comparisons


@register.filter(name='show_reading_validations_sequentials')
def show_reading_validations_sequentials(reading):
  sequentials = []
  for validation in reading.reading_validations.filter(validationType='sequential'):
    sequentials.append({
      'variable': validation.variable,
      'description': validation.description,
      'code': replace_env_variables(validation.code, reading.section.project_id),
    })
  return sequentials

@register.filter(name='json_response_formater')
def json_response_formater(response):
  if type(response) == QueryResponse:#'responseType' in response:
    if response.responseType != 'html':
      return json.dumps(json.loads(response.data), indent=4)
    else:
      return response.data
  else:
    return ''


@register.filter(name='to_list')
def to_list(object_dict):
  return object_dict.all()


@register.filter(name='filename')
def filename(file):
  return file.name.split("/")[::-1][0]


@register.filter(name='replace_env_vars')
def replace_env_vars(string, project_id):
  return replace_env_variables(string, project_id)


@register.filter(name='comparison_string')
def comparison_string(comparison_key):
  dictionary = {
    'exist': 'Existe',
    'is_null': 'Es Nulo',
    'is_text': 'Es Texto',
    'is_number': 'Es un Numero',
    'is_list': 'Es una Lista',
    'is_dictionary': 'Es un Diccionario',
    'equal': 'Igual a',
    'not_equal': 'Distinto a',
    'greather_than': 'Mayor a',
    'greather_equal_than': 'Mayor o igual a',
    'smaller_than': 'Menor a',
    'smaller_equal_than': 'Menor o igual a',
    'include': 'Contiene'
  }
  return dictionary[comparison_key]

