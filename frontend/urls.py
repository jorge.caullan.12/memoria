from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Project
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('<int:project_id>/tipo_automatizacion', views.menu_tipo_automatizacion),
    path('<int:project_id>/caja_negra', views.menu_caja_negra),
    path('<int:project_id>/caja_gris', views.menu_caja_gris),
    path('<int:project_id>/reportes', views.menu_reportes),

    path('<int:project_id>/caja_negra/imagenes', views.caja_negra_imagenes),
    path('<int:project_id>/caja_negra/imagenes/<int:test_id>', views.caja_negra_imagenes),
    path('<int:project_id>/caja_negra/casos_prueba', views.caja_negra_casos_prueba),
    path('<int:project_id>/caja_negra/casos_prueba/<int:test_id>', views.caja_negra_casos_prueba),

    path('<int:project_id>/caja_gris/lectura', views.caja_gris_lectura),
    path('<int:project_id>/caja_gris/lectura/<int:test_id>', views.caja_gris_lectura),
    path('<int:project_id>/caja_gris/lectura/secuencia', views.caja_gris_lectura_secuencia),
    path('<int:project_id>/caja_gris/lectura/secuencia/<int:test_id>', views.caja_gris_lectura_secuencia),
    path('<int:project_id>/caja_gris/escritura/consulta', views.caja_gris_escritura_consulta),
    path('<int:project_id>/caja_gris/escritura/consulta/<int:test_id>', views.caja_gris_escritura_consulta),
    path('<int:project_id>/caja_gris/escritura/secuencia', views.caja_gris_escritura_secuencia),
    path('<int:project_id>/caja_gris/escritura/secuencia/<int:test_id>', views.caja_gris_escritura_secuencia),

    path('<int:project_id>/environment/update', views.update_environment_variables),

    path('project/create', CreateView.as_view(model=Project, fields=['name'], success_url="/")),
    path('project/update/<int:pk>', UpdateView.as_view(model=Project, fields=['name'], success_url="/")),
    # path('project/delete/<int:pk>', DeleteView.as_view(model=Project, success_url="/")),

    path('<int:project_id>/reportes_generados', views.generate_reports)
]
