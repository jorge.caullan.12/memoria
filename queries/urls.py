from django.urls import path

from . import views

urlpatterns = [
    path('create', views.QueryCreate.as_view()),
    path('update/<int:pk>', views.QueryUpdate.as_view()),
    path('delete/<int:pk>', views.QueryDelete.as_view()),
    
    path('status/update/<int:pk>', views.QueryStatusUpdate.as_view()),

    path('section/create', views.SectionQueryCreate.as_view()),
    path('section/delete/<int:pk>', views.SectionQueryDelete.as_view()),

    path('save_request/<int:test_id>', views.save_request),
    path('execute_query/<int:test_id>', views.execute_query),

    # path('expected_data/create', views.QueryExpectedDataCreate.as_view()),
    # path('expected_data/update/<int:pk>', views.QueryExpectedDataUpdate.as_view()),
    # path('expected_data/delete/<int:pk>', views.QueryExpectedDataDelete.as_view()),

    # path('response/create', views.QueryResponseCreate.as_view()),
    # path('response/update/<int:pk>', views.QueryResponseUpdate.as_view()),

    # path('data/create', views.QueryDataCreate.as_view()),
    # path('data/update/<int:pk>', views.QueryDataUpdate.as_view()),
    # path('data/delete/<int:pk>', views.QueryDataDelete.as_view()),
]
