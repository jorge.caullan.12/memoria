# Generated by Django 2.2 on 2019-08-18 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('queries', '0018_auto_20190730_2111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='querydata',
            name='dataType',
            field=models.CharField(choices=[('header', 'Header'), ('form', 'Form'), ('json', 'Json')], default='form', max_length=10),
        ),
    ]
