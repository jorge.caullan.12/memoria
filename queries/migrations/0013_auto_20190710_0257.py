# Generated by Django 2.2 on 2019-07-10 06:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('queries', '0012_queryresponse_satisfied'),
    ]

    operations = [
        migrations.AlterField(
            model_name='query',
            name='domain',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='queries', to='frontend.EnvironmentValue'),
        ),
    ]
