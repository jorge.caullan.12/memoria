from django.contrib import messages
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Query, SectionQuery, QueryExpectedData, QueryResponse, QueryData
from django.contrib.messages.views import SuccessMessageMixin
from frontend.models import EnvironmentValue
from django.http import JsonResponse, HttpResponse
import requests, json, re

from .utils import *

comparison = {
  'exist': exist, 'is_null': is_null, 'is_text': is_text, 'is_number': is_number, 'is_list': is_list, 'is_dictionary': is_dictionary, 
  'equal': equal, 'not_equal': not_equal, 'greather_than': greather_than, 'greather_equal_than': greather_equal_than, 
  'smaller_than': smaller_than, 'smaller_equal_than': smaller_equal_than, 'include': include
}

def replace_env_variables(string, project_id):
  # for variable in re.findall(r"{\[(.*)\]}", string): #recorrer los evn_var para reemplazarlos
  # for variable in re.findall(r"{\[([^{]*)\]}", string):
  for variable in re.findall(r"{\[(((?!{\[).)*)\]}", string): #las frases que no incluyan {[
    env_var = EnvironmentValue.objects.filter(environmentType='variable', variable=variable[0], project_id=project_id)
    if env_var.count() > 0:
      string = string.replace('{['+str(variable[0])+']}', env_var.first().value)

  return string


def get_full_url(domain, path):
  if re.search(r'^http://', domain) or re.search(r'^https://', domain):
    url = domain
  else:
    url = 'http://'+domain

  if (not re.search(r'/$', domain)) and (not re.search(r'^/', path)):
    url = '{}/{}'.format(url, path)
  else:
    url = '{}{}'.format(url, path)

  return url


# # Create your views here.
class SectionQueryCreate(SuccessMessageMixin, CreateView):
  model = SectionQuery
  fields = ['title', 'project']
  success_message = "Seccion de consultas creada correctamente"


class SectionQueryDelete(DeleteView):
  model = SectionQuery



class QueryCreate(SuccessMessageMixin, CreateView):
  model = Query
  fields = ['title', 'section']
  success_message = "Consulta creada correctamente"


class QueryUpdate(UpdateView):
  model = Query
  # fields = ['title', 'method', 'status']
  fields = ['title', 'section']


class QueryDelete(DeleteView):
  model = Query
  def delete(self, request, *args, **kwargs):
    self.get_object().delete()
    messages.success(self.request, 'Consulta eliminada correctamente')
    return JsonResponse({'deleted': 'ok'})



class QueryStatusUpdate(UpdateView):
  model = Query
  fields = ['status']



def save_request(request, test_id):
  test = Query.objects.get(id=test_id)

  # print(request.POST)
  data = json.loads(request.POST['data'])
  # print(data)
  files = dict(request.FILES)
  # print(files)

  test.method = data['method']
  test.route = data['route']
  test.domain = EnvironmentValue.objects.get(value=data['domain'])
  # test.domain = data['domain']
  test.save()

  # HEADERS DATA
  headers = data['headers']
  current_headers = test.query_datas.filter(dataType='header')
  # limpieza de headers eliminados
  for existant_header in current_headers:
    if existant_header.variable not in headers:
      existant_header.delete()

  # añadir y editar headers
  for header in headers:
    header_query_data = current_headers.filter(variable=header).last()
    if not header_query_data:
      header_query_data = QueryData(dataType='header', variable=header, query=test)
    header_query_data.value = headers[header]['val']
    header_query_data.description = headers[header]['desc']
    header_query_data.save()


  # FORMS DATA
  body_form_datas = data['body_form_datas']
  current_body_form_datas = test.query_datas.filter(dataType='form')
  # limpieza de body_form_datas eliminados
  for existant_body_form_data in current_body_form_datas:
    if existant_body_form_data.variable not in body_form_datas:
      existant_body_form_data.delete()

  # añadir y editar body_form_datas
  for body_form_data in body_form_datas:
    form_query_data = current_body_form_datas.filter(variable=body_form_data).last()
    if not form_query_data:
      form_query_data = QueryData(dataType='form', variable=body_form_data, query=test)
    form_query_data.value = body_form_datas[body_form_data]['val']
    if body_form_data in files:
      file = request.FILES.get(body_form_data)
      print('file.name:', file.name)

      form_query_data.file.save(file.name, file)
    form_query_data.description = body_form_datas[body_form_data]['desc']
    form_query_data.save()


  # JSON DATA
  warning = ''
  try:
    if data['json_data']:
      #se intenta parsear la data entregada en formato json
      json.loads(replace_env_variables(data['json_data'], test.section.project_id)) 

    json_query_data = test.query_datas.filter(dataType='json').last()
    if not json_query_data:
      json_query_data = QueryData(dataType='json', query=test)
    json_query_data.json = data['json_data']
    json_query_data.save()
  except Exception as e:
    warning = 'Warning: No se guarda la data "Body as json" enviada, ya que no se logra parsear\n\nParse Error: {}'.format(e)


  # EXPECTED RESPONSES
  expected_responses = data['expected_responses']
  # añadir expected_responses (y eliminar los no modificados)
  deleted_data = test.query_expected_datas.all() #todos se 'eliminan' hasta que se compruebe su uso
  new_data = []
  for expected_response in expected_responses:
    #si ya existia la data esperada
    expected_data = test.query_expected_datas.filter(variable=expected_response['var'], comparison=expected_response['comp'], value=expected_response['val']).last()
    if expected_data:
      #el elemento aun existe, por lo que no hay que borrarlo
      deleted_data = deleted_data.exclude(id=expected_data.id)
    else:
      expected_data = QueryExpectedData(query=test)
      expected_data.variable = expected_response['var']
      expected_data.comparison = expected_response['comp']
      expected_data.value = expected_response['val']
      new_data.append(expected_data)
      # expected_data.save()

  #si se borra o se agrega alguna expected data
  if (deleted_data.count() > 0 or len(new_data) > 0) and hasattr(test, 'response'):
    query_response = test.response
    query_response.satisfied = None
    query_response.save()
  #limpieza de data que existia en un inicio y ya no
  deleted_data.delete()
  for expected_data in new_data:
    expected_data.save()

  if warning:
    # messages.warning(request, warning)
    return JsonResponse({'warning': warning})

  messages.success(request, 'Consulta guardada correctamente')
  return JsonResponse({'updated': 'ok'})



def get_query_response(test_id, request, send_message = True):
  test = Query.objects.get(id=test_id)

  # OBTENER LA DATA Y LOS HEADERS
  headers = {}
  for header in test.query_datas.filter(dataType='header'):
    headers[replace_env_variables(header.variable, test.section.project_id)] = replace_env_variables(header.value, test.section.project_id)

  files = {}
  form_datas = {}
  for form_data in test.query_datas.filter(dataType='form'):
    if form_data.file != '':
      files[replace_env_variables(form_data.variable, test.section.project_id)] = form_data.file
    else:
      form_datas[replace_env_variables(form_data.variable, test.section.project_id)] = replace_env_variables(form_data.value, test.section.project_id)

  json_data = {}
  json_query_data = test.query_datas.filter(dataType='json').last()
  if json_query_data and json_query_data.json: # para los casos en que no se use json (esta vacio)
    json_data = json.loads(replace_env_variables(json_query_data.json, test.section.project_id))
  #   for var in json_data:
  #     form_datas[var] = json_data[var]

  url = get_full_url(test.domain.value, test.route)

  if hasattr(test, 'response'):
    query_response = test.response
  else:
    query_response = QueryResponse(query=test)

  r = requests.request(test.method.upper(), replace_env_variables(url, test.section.project_id), headers=headers, data=form_datas, json=json_data, files=files)
  query_response.data = r.text

  test_satisfied = True
  try: #si la respuesta es un json
    json_obtained = json.loads(r.text)
    query_response.responseType = 'json'
  except:
    query_response.responseType = 'html'

  if query_response.responseType == 'json':
    # comparacion de respuestas esperadas
    for expected_data in test.query_expected_datas.all():
      if comparison[expected_data.comparison](
          replace_env_variables(expected_data.variable, test.section.project_id), 
          replace_env_variables(expected_data.value, test.section.project_id), 
          json_obtained, 
          request,
          send_message):
        expected_data.satisfied = True
      else:
        expected_data.satisfied = False
        test_satisfied = False
      expected_data.save()

  query_response.satisfied = test_satisfied
  query_response.save()

  return {'responseType': query_response.responseType, 'satisfied': test_satisfied, 'data': query_response.data}


def execute_query(request, test_id):
  r = get_query_response(test_id, request)

  response = {}
  if r['responseType'] == 'html':
    response['json'] = r['data']
    messages.warning(request, 'Las "Respuestas Esperadas" no fueron procesadas debido a que la respuesta no esta en formato JSON')
  else: #si es un json
    response['html'] = r['data']
    if r['satisfied']:
      messages.success(request, 'Consulta ejecutada correctamente')
    else:
      messages.warning(request, 'Ejecucion correcta<br>Problemas en la obtencion de algunas respuestas esperadas')

  #no es necesario aun que se envie la data, ya que se recarga la pagina, pero en un futuro probablemente no se haga de esa forma
  return JsonResponse(response)


