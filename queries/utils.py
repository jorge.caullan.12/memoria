from django.contrib import messages
import json

def get_variable_value(variables, json_obtained, request):
  # print('variables: {}'.format(variables))
  splited_variables = variables.split('->')
  value = json_obtained
  for var in splited_variables:
    var = var.strip()
    if (type(value)!=list and type(value)!=dict) or (var not in value):
      messages.error(request, 'Problema en respuesta esperada con variable: "{}"<br>La variable <b>{}</b>, no se encuentra en "{}"'.format(variables, var, json.dumps(value)))
      return False
    value = value[var]
  return value

def verify_digits( var, val, variable_value, request, send_message):
  if variable_value == False or not variable_value.replace('.','',1).isdigit():
    messages.if_send_error(request, 'El valor (obtenido) <b>{}</b> de la variable <b>{}</b>, no es un digito'.format(variable_value, var), send_message)
    return False
  elif not val.replace('.','',1).isdigit():
    messages.if_send_error(request, 'El valor (enviado) <b>{}</b> de la variable <b>{}</b>, no es un digito'.format(val, var), send_message)
    return False
  return True

def if_send_error(request, message, send_message = True):
  if send_message:
    messages.error(request, message.replace('\'','"')) #replace para evitar que se envien jsons que no podran ser leidos por el push
setattr(messages, 'if_send_error', if_send_error)





def exist(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False:
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no existe en la respuesta obtenida', send_message)
    return False

def is_text(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and type(variable_value) == str:
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> (de valor <b>{}</b>) no es Texto, es de tipo <b>{}</b>'.format(var, variable_value, type(variable_value).__name__), send_message)
    return False

def is_null(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value == None:
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> (de valor <b>{}</b>) no es Nula, es de tipo <b>{}</b>'.format(var, variable_value, type(variable_value).__name__), send_message)
    return False

def is_number(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and variable_value.replace('.','',1).isdigit():
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> (de valor <b>{}</b>) no es un Numero, es de tipo <b>{}</b>'.format(var, variable_value, type(variable_value).__name__), send_message)
    return False

def is_list(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and type(variable_value) == list:
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> (de valor <b>{}</b>) no es una Lista, es de tipo <b>{}</b>'.format(var, variable_value, type(variable_value).__name__), send_message)
    return False

def is_dictionary(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and type(variable_value) == dict:
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> (de valor <b>{}</b>) no es un Diccionario, es de tipo <b>{}</b>'.format(var, variable_value, type(variable_value).__name__), send_message)
    return False

def equal(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and str(variable_value) == str(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es igual a <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False

def not_equal(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value != False and str(variable_value) != str(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es distinta a <b>{}</b>'.format(var, variable_value), send_message)
    return False

def greather_than(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if not verify_digits(var, val, variable_value, request, send_message):
    return False
  if variable_value != False and float(variable_value) > float(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es mayor a <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False

def greather_equal_than(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if not verify_digits(var, val, variable_value, request, send_message):
    return False
  if variable_value != False and float(variable_value) >= float(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es moyor o igual a <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False

def smaller_than(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if not verify_digits(var, val, variable_value, request, send_message):
    return False
  if variable_value != False and float(variable_value) < float(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es menor a <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False

def smaller_equal_than(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if not verify_digits(var, val, variable_value, request, send_message):
    return False
  if variable_value != False and float(variable_value) <= float(val):
    return True
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no es menor o igual a <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False

def include(var, val, json_obtained, request, send_message):
  variable_value = get_variable_value(var, json_obtained, request)
  if variable_value and (type(variable_value)==list or type(variable_value)==dict or type(variable_value)==str):
    return val in variable_value
  else:
    messages.if_send_error(request, 'La variable <b>{}</b> no incluye <b>{}</b> (obtenido <b>{}</b>)'.format(var, val, variable_value), send_message)
    return False