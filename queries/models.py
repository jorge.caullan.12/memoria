from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.db import models
import os

def queries_directory_path(instance, filename):
  return "queries/data/{}/{}".format(instance.query_id, filename)

class OverwriteStorage(FileSystemStorage):
  def get_available_name(self, name, max_length=1000):
    if self.exists(name):
      os.remove(os.path.join(settings.MEDIA_ROOT, name))
    return name

# Create your models here.
class SectionQuery(models.Model):
  title = models.CharField(max_length=40)

  project = models.ForeignKey('frontend.Project', on_delete=models.CASCADE, related_name='section_queries')

  def __str__(self):
    return self.title

  def get_absolute_url(self):
    return "/{}/caja_gris/escritura/consulta".format(self.project_id)


class Query(models.Model):
  METHODS = (
    ('get', 'GET'),
    ('post', 'POST'),
    ('put', 'PUT'),
    ('patch', 'PATCH'),
    ('delete', 'Delete'),
  )
  LIST_STATUS = (
    ('inConstruction', 'InConstruction'),
    ('built', 'Built'),
  )

  title = models.CharField(max_length=60)
  method = models.CharField(
    max_length=10,
    choices=METHODS,
    default='get',
  )
  route = models.CharField(max_length=100, blank=True)
  status = models.CharField(
    max_length=15,
    choices=LIST_STATUS,
    default='inConstruction',
  )
  updated_at = models.DateTimeField(auto_now=True)

  # domain = models.CharField(max_length=40, blank=True)
  domain = models.ForeignKey('frontend.EnvironmentValue', blank=True, null=True, on_delete=models.SET_NULL, related_name='queries')
  section = models.ForeignKey(SectionQuery, on_delete=models.CASCADE, related_name='queries')

  def get_absolute_url(self):
    return "/{}/caja_gris/escritura/consulta/{}".format(self.section.project_id, self.id)


class QueryData(models.Model):
  DATA_TYPES = (
    ('header', 'Header'),
    ('form', 'Form'),
    ('json', 'Json'),
  )

  dataType = models.CharField(
    max_length=10,
    choices=DATA_TYPES,
    default='form',
  )
  variable = models.CharField(max_length=40, blank=True, null=True)
  value = models.CharField(max_length=40, blank=True, null=True)
  description = models.CharField(max_length=300, blank=True, null=True)
  json = models.CharField(max_length=5000, blank=True, null=True)
  file = models.FileField(upload_to=queries_directory_path, storage=OverwriteStorage(), blank=True)

  query = models.ForeignKey(Query, on_delete=models.CASCADE, related_name='query_datas')


class QueryResponse(models.Model):
  RESPONSE_TYPES = (
    ('html', 'Html'),
    ('json', 'Json'),
  )

  data = models.TextField()
  date = models.DateTimeField(auto_now=True)
  responseType = models.CharField(
    max_length=5,
    choices=RESPONSE_TYPES,
    default='json',
  )
  satisfied = models.BooleanField(blank=True, null=True)

  query = models.OneToOneField(Query, on_delete=models.CASCADE, related_name='response')


class QueryExpectedData(models.Model):
  variable = models.CharField(max_length=40)
  comparison = models.CharField(max_length=40)
  value = models.CharField(max_length=40, blank=True, null=True)
  satisfied = models.BooleanField(blank=True, null=True)

  query = models.ForeignKey(Query, on_delete=models.CASCADE, related_name='query_expected_datas')
