# Generated by Django 2.2 on 2019-06-22 03:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('readings', '0008_auto_20190620_2159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reading',
            name='status',
            field=models.CharField(choices=[('inConstruction', 'InConstruction'), ('built', 'Built')], default='inConstruction', max_length=10),
        ),
    ]
