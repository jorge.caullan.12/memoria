# Generated by Django 2.2 on 2019-07-12 20:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readings', '0017_reading_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reading',
            old_name='date',
            new_name='updated_at',
        ),
    ]
