from django.urls import path

from . import views

urlpatterns = [
    path('create', views.ReadingCreate.as_view()),
    path('update/<int:pk>', views.ReadingUpdate.as_view()),
    path('delete/<int:pk>', views.ReadingDelete.as_view()),
    
    path('status/update/<int:pk>', views.ReadingStatusUpdate.as_view()),

    path('section/create', views.SectionReadingCreate.as_view()),
    path('section/delete/<int:pk>', views.SectionReadingDelete.as_view()),

    path('test_database', views.test_database),
    path('test_sql', views.test_sql),

    path('validation/create', views.create_validation),
    path('validation/delete_remaining', views.delete_remaining_validations),

    path('execute/<int:test_id>', views.execute_test),

    # path('variables/create', views.ReadingVariablesCreate.as_view()),
    # path('variables/update/<int:pk>', views.ReadingVariablesUpdate.as_view()),

    # path('validation/create', views.ReadingValidationCreate.as_view()),
    # path('validation/update/<int:pk>', views.ReadingValidationUpdate.as_view()),
]
