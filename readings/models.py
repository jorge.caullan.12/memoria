from django.contrib.postgres.fields import ArrayField
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.db import models
import os

def reading_directory_path(instance, filename):
  return "readings/excels/{}/{}".format(instance.section_id, filename)

class OverwriteStorage(FileSystemStorage):
  def get_available_name(self, name, max_length=1000):
    if self.exists(name):
      os.remove(os.path.join(settings.MEDIA_ROOT, name))
    return name

# Create your models here.
class SectionReading(models.Model):
  title = models.CharField(max_length=40)

  project = models.ForeignKey('frontend.Project', on_delete=models.CASCADE, related_name='section_readings')

  def __str__(self):
    return self.title

  def get_absolute_url(self):
    return "/{}/caja_gris/lectura".format(self.project_id)


class Reading(models.Model):
  LIST_STATUS = (
    ('inConstruction', 'InConstruction'),
    ('built', 'Built'),
  )

  title = models.CharField(max_length=60)
  database = models.CharField(max_length=40, blank=True)
  service = models.CharField(max_length=40, blank=True)
  username = models.CharField(max_length=40, blank=True)
  password = models.CharField(max_length=40, blank=True)
  host = models.CharField(max_length=40, blank=True)
  port = models.CharField(max_length=10, blank=True)
  sql = models.TextField(blank=True)
  status = models.CharField(
    max_length=15,
    choices=LIST_STATUS,
    default='inConstruction',
  )
  excel = models.FileField(upload_to=reading_directory_path, storage=OverwriteStorage(), blank=True)
  updated_at = models.DateTimeField(auto_now=True)

  section = models.ForeignKey(SectionReading, on_delete=models.CASCADE, related_name='readings')

  def get_absolute_url(self):
    return "/{}/caja_gris/lectura/{}".format(self.section.project_id, self.id)


class ReadingVariables(models.Model):
  variables = ArrayField(
    models.CharField(max_length=60, blank=True),
    size=50,
  )

  reading = models.ForeignKey(Reading, on_delete=models.CASCADE, related_name='reading_variables')


class ResponseRow(models.Model):
  order = models.IntegerField()
  data = ArrayField(
    models.CharField(max_length=100, blank=True),
    size=200,
  )

  reading_variables = models.ForeignKey(ReadingVariables, on_delete=models.CASCADE, related_name='response_rows')


class ReadingValidation(models.Model):
  VALIDATION_TYPES = (
    ('comparison', 'Comparison'),
    ('sequential', 'Sequential'),
  )

  validationType = models.CharField(
    max_length=15,
    choices=VALIDATION_TYPES,
    default='comparison',
  )
  variable = models.CharField(max_length=40)
  description = models.CharField(max_length=400)
  code = models.TextField()
  # trueColor = models.CharField(max_length=10, default="28a745") #green
  trueColor = models.CharField(max_length=10, blank=True) #green
  trueComment = models.CharField(max_length=200, blank=True)
  # falseColor = models.CharField(max_length=10, default="dc3545") #red
  falseColor = models.CharField(max_length=10, blank=True) #red
  falseComment = models.CharField(max_length=200, blank=True)

  reading = models.ForeignKey(Reading, on_delete=models.CASCADE, related_name='reading_validations')
