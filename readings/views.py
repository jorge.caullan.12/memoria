from django.contrib import messages
from django.core.files.base import ContentFile
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from frontend.models import Project, EnvironmentValue
from .models import SectionReading, Reading, ReadingVariables, ReadingValidation, ResponseRow
from queries.views import replace_env_variables
from django.contrib.messages.views import SuccessMessageMixin


from django.http import JsonResponse, HttpResponse
import cx_Oracle, psycopg2, re, datetime, os
from openpyxl import Workbook, load_workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font, Color, Alignment, PatternFill
from openpyxl.comments import Comment
from pprint import pprint


# Create your views here.
class SectionReadingCreate(SuccessMessageMixin, CreateView):
  model = SectionReading
  fields = ['title', 'project']
  success_message = "Seccion de pruebas de lectura creada correctamente"


class SectionReadingDelete(DeleteView):
  model = SectionReading



class ReadingCreate(SuccessMessageMixin, CreateView):
  model = Reading
  fields = ['title', 'section']
  success_message = "Prueba de lectura creada correctamente"


class ReadingUpdate(UpdateView):
  model = Reading
  fields = ['title', 'section']


class ReadingDelete(DeleteView):
  model = Reading
  def delete(self, request, *args, **kwargs):
    self.get_object().delete()
    messages.success(self.request, 'Test eliminado correctamente')
    return JsonResponse({'deleted': 'ok'})


class ReadingStatusUpdate(UpdateView):
  model = Reading
  fields = ['status']



def test_database(request):
  database_type = request.POST['database_type']
  username = request.POST['username']
  password = request.POST['password']
  service = request.POST['service']
  host = request.POST['host']
  if host == '':
    host = 'localhost'
  port = request.POST['port']
  test = Reading.objects.get(id=request.POST['test_id'])

  if database_type == 'oracle':
    db_dsn = cx_Oracle.makedsn(host, port, sid=service)
    db_pool = cx_Oracle.SessionPool(dsn=db_dsn, min=1, max=15, increment=1, homogeneous=False)
    conexion = db_pool.acquire(user=username, password=password)
  elif database_type == 'postgresql':
    if port == '':
      port = '5432'
    psycopg2.connect(user = username, password = password, host = host, port = port, database = service)
  else:
    raise 'Base de datos desconocida'
  
  test.database = database_type
  test.username = username
  test.password = password
  test.service = service
  test.host = host
  test.port = port
  test.save()
  return JsonResponse({'connection': 'ok'})


def test_sql(request):
  test = Reading.objects.get(id=request.POST['test_id'])
  original_query = request.POST['query']
  test_query_anterior = test.sql

  if not re.search(r"^select\s*", original_query.lower()):
    raise Exception('La query debe comenzar con SELECT')

  query = original_query
  query = replace_env_variables(query, test.section.project_id)

  variables = []
  all_variables = {}
  query_data = re.sub(r';\s*$', '', query.lower()) #query en minusculas y sin el ; del final
  all_tables = re.findall(r"from\s*(\S*)\s?", query_data) + re.findall(r"inner join\s*(\S*)\s?", query_data)

  if test.database == 'oracle':
    db_dsn = cx_Oracle.makedsn(test.host, test.port, sid=test.service)
    db_pool = cx_Oracle.SessionPool(dsn=db_dsn, min=1, max=15, increment=1, homogeneous=False)
    conn = db_pool.acquire(user=test.username, password=test.password)

  elif test.database == 'postgresql':
    conn = psycopg2.connect(user = test.username, password = test.password, host = test.host, port = test.port, database = test.service)

  # a partir de la bd indicada, debe obtenerse las variables: response, all_variables, variables
  cur = conn.cursor()

  cur.execute(query)
  response = cur.fetchall()

  for table in all_tables:
    if test.database == 'oracle':
      cur.execute("SELECT * FROM {}".format(table))
    elif test.database == 'postgresql':
      cur.execute("SELECT * FROM {};".format(table))
    for column in cur.description:
      if table not in all_variables:
        all_variables[table] = []
      all_variables[table].append("{}".format(column[0].lower()))
      variables.append("{}.{}".format(table, column[0].lower()))

  cur.close()
  conn.close()

  resp_all_variables = all_variables

  # fix de all_variables, para el caso en que se pidan datos especificos en el SELECT ## resp_all_variables solo se usa para ver las variables
  if not re.search(r"^select\s\*\s", query_data):
    resp_all_variables = {}
    variables = []
    columns = re.findall(r"select\s(.*)\sfrom", query_data)[0]
    requested_variables = columns.replace(' ','').split(',')
    for var in requested_variables:
      requested_table_var = var.split('.')

      if len(requested_table_var) > 1: #variables que ya tienen tabla en su nombre
        rtable = requested_table_var[0]
        rvar = requested_table_var[1]
        #se agregan a las variables y a la respuesta all variables
        if rtable not in resp_all_variables:
          resp_all_variables[rtable] = []
        resp_all_variables[rtable].append(rvar)
        variables.append("{}.{}".format(rtable, rvar))
      else: #si la variable es unica (y no se entrega nombre de la tabla)
        rvar = requested_table_var[0]
        for table in all_variables:
          if rvar in all_variables[table]:
            # print(rvar, table)
            #si es de la tabla actual
            if table not in resp_all_variables:
              resp_all_variables[table] = []
            resp_all_variables[table].append(rvar)
            variables.append("{}.{}".format(table, rvar))
            continue

  if test.reading_variables.count() > 0:
    test_variables = test.reading_variables.last()
    test_variables.variables = variables
  else:
    test_variables = ReadingVariables(variables=variables, reading=test)

  test_variables.save()
  test.sql = original_query
  test.save()


  if test_query_anterior != original_query: #si se cambio la query, volver a generar las respuestas
    test.reading_variables.last().response_rows.all().delete() # limpieza de las filas obtenidas anteriormente

    reading_variables = test.reading_variables.last()
    order = 0
    for row in response:
      data = list(row)
      ResponseRow.objects.create(order=order, data=data, reading_variables=reading_variables)
      order += 1

  return JsonResponse(resp_all_variables)


def create_validation(request):
  test = Reading.objects.get(id=request.POST['test_id'])

  validation_id = request.POST['id']

  if validation_id != '0':
    validation = ReadingValidation.objects.get(id=validation_id)
  else:
    validation = ReadingValidation()

  validation.validationType = request.POST['validation_type']
  validation.variable = request.POST['variable']
  validation.description = request.POST['description']
  validation.code = request.POST['code']
  validation.trueColor = request.POST['ok_color']
  validation.trueComment = request.POST['ok_comment']
  validation.falseColor = request.POST['false_color']
  validation.falseComment = request.POST['false_comment']
  validation.reading = test

  validation.save()

  return JsonResponse({'created': 'ok'})


def delete_remaining_validations(request):
  test = Reading.objects.get(id=request.POST['test_id'])

  ids = request.POST.getlist('ids[]')

  for validation in test.reading_validations.all():
    if str(validation.id) not in ids:
      validation.delete()

  return JsonResponse({'deleted': 'ok'})


def get_test_workshet(test_id, wb = None):
  test = Reading.objects.get(id=test_id)
  project_id = test.section.project_id

  reading_variables = test.reading_variables.last()
  db_variables = reading_variables.variables
  response_rows = reading_variables.response_rows.all()

  comparison_validations = test.reading_validations.filter(validationType='comparison')
  sequential_validations = test.reading_validations.filter(validationType='sequential')

  functions = {}
  #por cada validacion definir la funcion validacion_id
  for validation in comparison_validations:
    validation_function = ('def comparison_validation_{}(variables):\n{}'.format(validation.id, validation.code)).replace('\n', '\n\t') #definir la funcion
    # @(\S*) #obtener todos los caracteres que no son espacios... @([^\s:=!><]*) #obtener todos los caracteres que no sean: space : = ! > <
    validation_function = replace_env_variables(validation_function, project_id)
    validation_function = re.sub(r'@([\w\d_.]*)', r'variables["\1"]', validation_function) #obtener solo los caracteres que sean: word digit _ . 
    exec(validation_function, globals(), functions)

  for validation in sequential_validations:
    validation_function = ('def sequential_validation_{}(variables, variables_anteriores):\n{}'.format(validation.id, validation.code)).replace('\n', '\n\t') #definir la funcion
    validation_function = replace_env_variables(validation_function, project_id)
    validation_function = re.sub(r'@([\w\d_.]*)_anterior', r'variables_anteriores["\1"]', validation_function)
    validation_function = re.sub(r'@([\w\d_.]*)', r'variables["\1"]', validation_function)
    exec(validation_function, globals(), functions)

  variables = {}
  variables_anteriores = {}

  if wb == None:
    wb = Workbook()
    wb.remove_sheet(wb.active)
    # wb.remove_sheet(wb.get_sheet_by_name('Sheet'))

  sheet_title = replace_env_variables(test.title, project_id)
  ws = wb.create_sheet(sheet_title)

  font = Font(bold=True, color='ffffff')
  fill_header = PatternFill(fill_type='solid', fgColor='1b365d')
  alignment = Alignment(horizontal='center', wrap_text=True)

  for i in range(len(db_variables)):
    cell = ws.cell(row=1, column=(i+1))
    cell.font = font
    cell.fill = fill_header
    cell.alignment = alignment
    cell.value = db_variables[i]

  # por cada fila obtener el valor de cada variable
  for j in range(len(response_rows)):
    # asignar los valores de cada variable en la fila actual
    row = response_rows.filter(order=j).first()
    for i in range(len(db_variables)):
      value = row.data[i]
      if value == None:
        value = ""
      if value.replace('.','',1).isdigit():
        value = float(value)
      variables[db_variables[i]] = value
      #se anota el valor de cada casilla en el excel
      cell = ws.cell(row=(j+2), column=(i+1))
      cell.value = value

    #validaciones de pruebas comparativas
    for validation in comparison_validations:
      cell = ws.cell(row=(j+2), column=(db_variables.index(validation.variable)+1))
      color = ''
      comment = ''
      if functions['comparison_validation_{}'.format(validation.id)](variables):
        color = validation.trueColor
        comment = validation.trueComment
        for variable in re.findall(r'@([\w\d_.]*)', validation.trueComment):
          comment = comment.replace("@"+variable, str(variables[variable]))
      else:
        color = validation.falseColor
        comment = validation.falseComment
        for variable in re.findall(r'@([\w\d_.]*)', validation.falseComment):
          comment = comment.replace("@"+variable, str(variables[variable]))

      if color:
        cell.fill = PatternFill(fill_type='solid', fgColor=color)
      if comment:
        comment = replace_env_variables(comment, project_id)
        cell.comment = Comment(comment, "Testing automatizado")

    #validaciones de pruebas secuenciales
    if j>0:
      # asignar los valores de cada variable de la fila anterior
      row_anterior = response_rows.filter(order=(j-1)).first()
      for i in range(len(db_variables)):
        value = row_anterior.data[i]
        if value == None:
          value = ""
        if value.replace('.','',1).isdigit():
          value = float(value)
        variables_anteriores[db_variables[i]] = value

      # si no se esta en la primera file, se realiza la validacion sequencial
      for validation in sequential_validations:
        cell = ws.cell(row=(j+2), column=(db_variables.index(validation.variable)+1))
        color = ''
        comment = ''
        if functions['sequential_validation_{}'.format(validation.id)](variables, variables_anteriores):
          color = validation.trueColor
          comment = validation.trueComment
          for variable in re.findall(r'@([\w\d_.]*)', validation.trueComment):
            if re.search(r'_anterior$', variable):
              b_variable = re.sub(r'_anterior$', '', variable)
              comment = comment.replace("@"+variable, str(variables_anteriores[b_variable]))
            else:
              comment = comment.replace("@"+variable, str(variables[variable]))
        else:
          color = validation.falseColor
          comment = validation.falseComment
          for variable in re.findall(r'@([\w\d_.]*)', validation.falseComment):
            if re.search(r'_anterior$', variable):
              b_variable = re.sub(r'_anterior$', '', variable)
              comment = comment.replace("@"+variable, str(variables_anteriores[b_variable]))
            else:
              comment = comment.replace("@"+variable, str(variables[variable]))

        if color:
          cell.fill = PatternFill(fill_type='solid', fgColor=color)
        if comment:
          comment = replace_env_variables(comment, project_id)
          cell.comment = Comment(comment, "Testing automatizado")
    else:
      #se autovalida la primera fila ya que no hay comparacion con variables anteriores
      for validation in sequential_validations:
        cell = ws.cell(row=(j+2), column=(db_variables.index(validation.variable)+1))
        cell.fill = PatternFill(fill_type='solid', fgColor=validation.trueColor)

  column_width = 30
  for column in range(1, ws.max_column+1):
    ws.column_dimensions[get_column_letter(column)].width = column_width

  filename = "test{}_{}.xlsx".format(test.id, datetime.datetime.now().strftime('%d%m%Y_%I%p'))

  # hacer una copia del wb para que en la base de datos solo se guarde el sheet del test actual
  wb.save(filename=filename) #temporal completo

  wb_copy = load_workbook(filename)
  names = wb_copy.sheetnames
  for i in range(len(names)):
    if names[i] != sheet_title:
      wb_copy.remove(wb_copy[names[i]])

  wb_copy.save(filename=filename) #temporal filtrado

  f = open(filename, 'rb')
  file = ContentFile(f.read())
  f.close()
  test.excel.save(filename, file)
  test.save()

  response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
  response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
  wb.save(response)

  os.remove(filename) #remove de archivo temporal
  # messages.success(request, 'Prueba de lectura ejecutada correctamente')

  return (wb, response)

def execute_test(request, test_id):
  test = Reading.objects.get(id=test_id)
  if test.reading_variables.count() > 0:
    wb, response = get_test_workshet(test_id)
    messages.success(request, 'Prueba de lectura ejecutada correctamente')
    return response
  else:
    messages.error(request, 'La consulta SQL no ha sido probada con exito')
    return JsonResponse({'status':'false', 'error':'el test no existe'}, status=500)